package com.example.huaweitest;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] s =str.split(" ");
        int length = s[s.length-1].length();
        System.out.println(length);
    }
}
