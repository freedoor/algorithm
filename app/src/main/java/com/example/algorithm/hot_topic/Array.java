package com.example.algorithm.hot_topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Array {
    //    最大子数组和
    public int maxSubArray(int[] nums) {
        int res = nums[0];
        for(int i = 1; i < nums.length; i++) {
            nums[i] += Math.max(nums[i - 1], 0);
            res = Math.max(res, nums[i]);
        }
        return res;
    }

    //    合并区间
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals, (p, q) -> p[0] - q[0]); // 按照左端点从小到大排序
        List<int[]> ans = new ArrayList<>();
        for (int[] p : intervals) {
            int m = ans.size();
            if (m > 0 && p[0] <= ans.get(m - 1)[1]) { // 可以合并
                ans.get(m - 1)[1] = Math.max(ans.get(m - 1)[1], p[1]); // 更新右端点最大值
            } else { // 不相交，无法合并
                ans.add(p); // 新的合并区间
            }
        }
        return ans.toArray(new int[ans.size()][]);
    }

    //    轮转数组
    public void rotate(int[] nums, int k) {
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);
    }
    public void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start += 1;
            end -= 1;
        }
    }

    //    自身以外数组乘积
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] dp = new int[n]; // 存储左侧所有元素乘积
        int[] pd = new int[n]; // 存储右侧所有元素的乘积
        // 初始化
        dp[0] = 1;
        pd[n - 1] = 1;
        // 计算（左侧所有元素的乘积）
        for (int i = 1; i < n; i++) {
            dp[i] = dp[i - 1] * nums[i - 1];
        }
        // 计算右侧所有元素的乘积）
        for (int i = n - 2; i >= 0; i--) {
            pd[i] = pd[i + 1] * nums[i + 1];
        }

        // 构造最终结果
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            result[i] = dp[i] * pd[i];
        }

        return result;
    }
}
